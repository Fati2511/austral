class Bicicleta {
    constructor(id, color, modelo, ubicacion = []){
        this.id = id;
        this.color = color;
        this.modelo = modelo;
        this.ubicacion = ubicacion;
    }

    static allBicis = [];

    toString(){
        return `Bicileta\nid: ${this.id}\ncolor: ${this.color}`;
    }

    static add(bici){
        this.allBicis.push(bici)
    }

    static deleteById(idBici) {
        let indexBici = this.allBicis.findIndex(bici => bici.id == idBici);
        if(indexBici === -1) {
            throw new Error("CANT DELETE")
        }
        this.allBicis.splice(indexBici, 1);
    }
}

var a = new Bicicleta(1, 'rojo', 'montaña', [223.321, 224.432]);
var b = new Bicicleta(2, 'verde', 'urbana', [223.321, 224.432]);
var c = new Bicicleta(3, 'azul', 'montaña', [213.321, 2234.432]);
var d = new Bicicleta(4, 'gris', 'urbana', [225.321, 223.432]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);

module.exports = Bicicleta;