# Primera entrega del curso Desarrollo del lado del Servidor
Se trata de un sencillo proyecto web centrado en el uso del framework express

Cuenta con una API cuyos endpoints son:
- http://localhost:3000/api/bicicletas (GET)

- http://localhost:3000/api/bicicletas/create  (POST)

- http://localhost:3000/api/bicicletas/delete (POST)

- http://localhost:3000/api/bicicletas/update (POST)

El comando _npm start_ o _npm run devstart_ corre el proyecto
